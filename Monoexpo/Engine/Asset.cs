﻿using System;

using Microsoft.Xna.Framework.Content;

namespace Monoexpo.Engine
{
    /// <summary>
    /// The Asset class, this is used for you to manage Textures or anything else of content.
    /// </summary>
    /// <typeparam name="ContentType">The content type of which to manage.</typeparam>
    public class Asset<ContentType>
    {
        public readonly string Name;

        public ContentType Content { get; private set; }
        
        /// <param name="name">The Content AssetName.</param>
        public Asset(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Loads the asset Content, if not already loaded, and returns it.
        /// </summary>
        /// <param name="content">The game content manager.</param>
        /// <returns>The asset content.</returns>
        public ContentType Load(ContentManager content)
        {
            if (Content != null)
                throw new Exception("The asset \"" + Name + "\" has already been loaded!");

            Content = content.Load<ContentType>(Name);

            return Content;
        }
    }
}
