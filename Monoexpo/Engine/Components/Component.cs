﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Monoexpo.Engine.Entities;

namespace Monoexpo.Engine.Components
{
    /// <summary>
    /// The component abstract class is used to create every component.
    /// </summary>
    public abstract class Component
    {
        public readonly Entity Father;
        
        /// <param name="father">The entity in which the component is on.</param>
        public Component(Entity father)
        {
            Father = father;
        }

        public virtual void Initialize()
        { }

        public virtual void LoadContent(ContentManager content)
        { }

        public virtual void Update(GameTime gameTime)
        { }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        { }
    }
}
