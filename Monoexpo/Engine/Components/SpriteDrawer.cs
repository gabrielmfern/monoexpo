﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Monoexpo.Engine.Entities;

namespace Monoexpo.Engine.Components
{
    /// <summary>
    /// The Sprite Drawer component, is used to draw sprites.
    /// </summary>
    public class SpriteDrawer : Component
    {
        public readonly bool CanLoadSprite;

        public readonly Asset<Texture2D> Sprite;
        public readonly Color SpriteColor;

        public readonly SpriteSortMode SpriteSortMode;

        public SpriteDrawer(Entity father, Asset<Texture2D> sprite, Color color, SpriteSortMode ssm = SpriteSortMode.Deferred, bool loadSprite = true) : base(father)
        {
            Sprite = sprite;
            SpriteSortMode = ssm;
            CanLoadSprite = loadSprite;
            SpriteColor = color;
        }

        public override void Initialize()
        {
            if (Father.GetComponent<Transform>() == null)
                throw new Exception("The Transform component is required for the Sprite to be drawn!");

            base.Initialize();
        }

        public override void LoadContent(ContentManager content)
        {
            if (CanLoadSprite)
                Sprite.Load(content);

            base.LoadContent(content);
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (Sprite.Content == null)
                throw new Exception("The Sprite texture needs to be lodaded, so that the sprite can be drawn!");

            Transform transform = Father.GetComponent<Transform>();

            spriteBatch.Begin(SpriteSortMode);
            spriteBatch.Draw(Sprite.Content, new Rectangle((int)transform.Position.X, (int)transform.Position.Y, (int)transform.Scale.X, (int)transform.Scale.Y), SpriteColor);
            spriteBatch.End();

            base.Draw(spriteBatch, gameTime);
        }
    }
}
