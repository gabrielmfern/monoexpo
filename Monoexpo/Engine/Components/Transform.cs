﻿using Microsoft.Xna.Framework;

using Monoexpo.Engine.Entities;

namespace Monoexpo.Engine.Components
{
    /// <summary>
    /// The Transform component is used to manager stuff like position and scale.
    /// </summary>
    public class Transform : Component
    {
        public Vector2 Position, Scale;

        public Transform(Entity father, Vector2 pos, Vector2 scl) : base(father)
        {
            Position = pos;
            Scale = scl;
        }

        /// <summary>
        /// Sets the Transform's position.
        /// </summary>
        /// <param name="position">The new position.</param>
        public void SetPosition(Vector2 position)
        {
            Position = position;
        }

        /// <summary>
        /// Sets the Transform's scale.
        /// </summary>
        /// <param name="scale">The new scale.</param>
        public void SetScale(Vector2 scale)
        {
            Scale = scale;
        }
    }
}
