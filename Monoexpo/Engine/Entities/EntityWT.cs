﻿using Microsoft.Xna.Framework;

using Monoexpo.Engine.Components;

namespace Monoexpo.Engine.Entities
{
    /// <summary>
    /// This is just a modification of the normal Entity class, just adds a Transform property that makes it a little more accessble.
    /// </summary>
    public abstract class EntityWT : Entity
    {
        public Transform ETransform { get; private set; }

        public EntityWT(Vector2 pos, Vector2 scl) : base()
        {
            AddComponent<Transform>(new object[] { pos, scl });
            ETransform = GetComponent<Transform>();
        }

        /// <summary>
        /// Sets the Transform components position.
        /// </summary>
        /// <param name="NewPosition">The new position.</param>
        public void SetPosition(Vector2 NewPosition)
        {
            ETransform.SetPosition(NewPosition);
        }

        /// <summary>
        /// Sets the Transform components scale.
        /// </summary>
        /// <param name="NewScale"></param>
        public void SetScale(Vector2 NewScale)
        {
            ETransform.SetScale(NewScale);
        }
    }
}
