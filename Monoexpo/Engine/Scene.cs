﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Monoexpo.Engine
{
    public abstract class Scene
    {
        protected GraphicsDeviceManager Graphics;
        protected ContentManager PrivateContent;
        protected SpriteBatch SpriteBatch;
        
        public Scene(GraphicsDeviceManager graphics, ContentManager content, string rootDirectory = null)
        {
            if (rootDirectory == null)
                rootDirectory = content.RootDirectory;

            PrivateContent = new ContentManager(content.ServiceProvider, rootDirectory);
            Graphics = graphics;
        }

        public virtual void Initialize() { }

        public virtual void LoadContent()
        {
            SpriteBatch = new SpriteBatch(Graphics.GraphicsDevice);
        }

        public void UnloadContent()
        {
            PrivateContent.Unload();
        }

        public virtual void Update(GameTime gameTime) { }

        public virtual void Draw(GameTime gameTime) { }
    }
}
