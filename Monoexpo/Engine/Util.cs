﻿namespace Monoexpo.Engine
{
    public static class Util
    {
        /// <summary>
        /// Sets a specific property inside a object.
        /// </summary>
        /// <typeparam name="T">The object class Type.</typeparam>
        /// <param name="obj">The object.</param>
        /// <param name="property">The property name.</param>
        /// <param name="value">The new property value.</param>
        public static void SetPropertyTo<T>(T obj, string property, object value)
        {
            typeof(T).GetProperty(property).SetValue(obj, value);
        }
    }
}
