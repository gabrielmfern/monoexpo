# Monoexpo

## Por quê?

Monoexpo existe para ajudá-lo no desenvolvimento de jogos utilizando MonoGame.

## Quer acompanhar o desenvolvimento?

Todos os sábados/domingos eu faço uma stream programando em algo,
esse algo pode ser o Monoexpo, então me siga lá para não perder nada,
só clicar [aqui](https://www.twitch.tv/00datto00).

## Quem sou eu?

Meu nome é Gabriel, eu tenho 13 anos, comecei à criar esta
Engine open-source para poder ajudar pessoas no desenvolvimento de jogos.

## Quer me encorajar?

Se quiser me encorajar, você tanto pode se inscrever em meu canal na Twitch,
assim como pode se tornar um de meus [patrões](https://www.patreon.com/tots1).