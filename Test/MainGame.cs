﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Monoexpo.Engine;

namespace Test
{
    public class GameScene : Scene
    {
        public GameScene(GraphicsDeviceManager graphics, ContentManager content) : base(graphics, content) { }
    }

    public class MainGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public MainGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        
        protected override void Initialize()
        {
            SceneManager.Instance.Initialize(Content, graphics, new SceneConstructor[]
            {
                new SceneConstructor(new object[] {  }, typeof(GameScene))
            });

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            SceneManager.Instance.LoadContent();
        }
        
        protected override void UnloadContent()
        {
            SceneManager.Instance.UnloadContent();
        }
        
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            SceneManager.Instance.Update(gameTime);

            base.Update(gameTime);
        }
        
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            SceneManager.Instance.Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}
